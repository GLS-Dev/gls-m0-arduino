# 1 - Install Arduino IDE
#### [Download](https://www.arduino.cc/en/Main/Software) the Arduino IDE if not installed.

![Arduino IDE download](README IMAGES/1.png)

# 2 - Open Boards Manager
#### Tools -> Board -> Boards Manager

![Boards Manager Location](README IMAGES/2.png)

# 3 - Install Arduino SAMD Boards
#### Search "SAMD" or scroll down.

![Arduino SAMD Boards](README IMAGES/3.png)

# 4 - Open Arduino Preferences
#### File -> Preferences

![Preferences Location](README IMAGES/4.png)

# 5A - Add Additional Boards Manager URL
#### [http://griffin.lighting/code/package_gls_index.json](http://griffin.lighting/code/package_gls_index.json)

![Additional Boards URL](README IMAGES/5.png)

# 5B - Restart The Arduino IDE

# 6A - Open Boards Manager
#### Tools -> Board -> Boards Manager

![Boards Manager Location](README IMAGES/2.png)

# 6B - Install GLS Boards
#### Found under "Contributed" tab/Searching "GLS"

![GLS Boards](README IMAGES/6.png)

# 7 - Select GLS DMfleX Board
#### Tools -> Board -> GLS SAMD -> DMfleX

![DMfleX](README IMAGES/7.png)

# 8 - Open DMfleX Example
#### File -> Examples -> LXSAMD21DMX -> DMfleX

This file is commented to help you get started. 
Under the heading "BASIC CONFIG:" are settings to change the output DMX addresses and intensity.

**NOTE:** When programming the board should not be connected to external power. The board is capable of running from the USB C connector. 

![Examples](README IMAGES/8.png)

# 9 - Attach Board/Select Port
#### Tools -> Port

Depending on your OS the board will appear here under different names.
No additional drivers should be required for modern operating systems.

**NOTE:** Some cheaper USB C to USB A cables may only function in one orientation. 
If the board is not detected please flip the connector over.

![Port](README IMAGES/9.png)

# 10 - Uploading

To upload code to the DMfleX board double click its reset button to put it into bootloader mode.
Alternatively, click the upload button once to force the board into bootloader mode. If an error occurs, press upload a second time.

**NOTE:** It is recomended that you unplug the board from your computer before you test its DMX output functionality.